import Form from "./Form.js";
import Element from "./Elements.js";
import {VisitFormDantist, VisitFormTherapist, VisitFormCardiologist} from "./VisitForm.js";
 


export default class SelectForm extends Form{
    constructor(classes, id){
        super(classes, id)
    }
    createFields(){
        let optionAll = new Element('option', [''], '', 'All').createElement()
        optionAll.selected = true;

        let optionDantist = new Element('option', [''], '', 'Dantist').createElement()
        optionDantist.value = 'Dantist'

        let optionCardiologist = new Element('option', [''], '', 'Cardiologist').createElement()
        optionCardiologist.value = 'Cardiologist'


        let optionTerapevt = new Element('option', [''], '', 'Terapevt').createElement()
        optionTerapevt.value = 'Terapevt'

        let fragment = document.createDocumentFragment()

        fragment.append(optionAll,optionDantist, optionCardiologist, optionTerapevt)

        
        
        let form = document.createElement('form')
        
        let select = new Element('select', ['select_doctor', 'form-select', 'mt-2'], 'selectDoc', fragment).createElement()

        document.body.addEventListener('click', (event)=>{
              
            if(event.target.id === 'select_modal'){
                document.getElementById('selectDoc').value = 'All'
                event.target.classList.remove('active')
               
                if(document.getElementById('form_doctor')){
                    document.getElementById('form_doctor').remove()
                }
                event.target.remove()
            }
            else if(event.target.id === 'editmodal'){
                event.target.remove()
            }
            
        })
        select.addEventListener('change', (event)=>{
            
            let doc = event.target.value
            if(doc === 'Dantist'){
                form.remove()
    
                form = new VisitFormDantist(['test'], 'form_doctor').render()
                select.after(form)
            }
          else if(doc === 'Terapevt'){
                form.remove()
                form = new VisitFormTherapist(['test1'], 'form_doctor').render()
                select.after(form)
            }
            else if(doc === 'Cardiologist'){
                form.remove()
                form = new VisitFormCardiologist(['test2'], 'form_doctor').render()
                select.after(form)
            }
            else{
                form.remove()
                
            }
        })


        return select
    }
}   