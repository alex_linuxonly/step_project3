
export var token;

export default class Request {
    constructor(url) {
        this.url = url
    }
    post2(data) {
        return fetch(this.url + 'cards', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(data)
        })


    }
    postLogin(data) {
        return (axios({
            method: "post",
            url: this.url + 'cards/login',
            data: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        })).then(({data}) => {
            
            token = data
            return data
        })

    }
    get(entity) {
         axios({
            method: "get",
            url: this.url + entity,
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            }
        })

    }
    delete(id) {
        return fetch(this.url + 'cards/' + id, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${token}`
            },
        })
    }

    editCard(id, data) {
       return fetch(this.url + 'cards/' + id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({data})
            })
    }
}
