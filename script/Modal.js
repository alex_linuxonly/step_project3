import Element from "./Elements.js";


import CONSTANTS from "./constants.js";

const { ROOT } = CONSTANTS;

export default class Modal{
    constructor(id, classes, text){
        this.id = id;
        this.classes = classes;
        this.text = text;
        this.content = this.createContent() 
    }
    createContent() {
        return '' ;
    }

    render(){

        let closeBtn = new Element('span', ['close'], '', '&times;').createElement()

        let fragment = document.createDocumentFragment()
        
        fragment.append(closeBtn, this.text,  this.content)

        let modalContent = new Element('div', ['modal-content'], 'modalWnd', fragment).createElement()

        let modal = new Element('div', this.classes, this.id,  modalContent ).createElement()


        this._modal = modal;
        ROOT.append(this._modal);

        closeBtn.addEventListener('click', ()=>{
            this.closeModal()
        })
    }

    openModal(){
        
        this._modal.classList.add('active')

    }
    closeModal(){
        this._modal.remove()
        // this._modal.classList.remove('active')
    }
}

