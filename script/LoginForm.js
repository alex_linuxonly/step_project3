import Form from "./Form.js";
import Element from "./Elements.js";
import Request from "./Request.js";
import CONSTANTS from './constants.js';
import Card from "./Card.js";
const { URL } = CONSTANTS

export default class LogInForm extends Form{
    constructor(classes, id){
        super(classes, id)
    }

    createFields(){
        let EmailLabel = new Element('label', ['1'], '', 'Email address').createElement()
        EmailLabel.for = "exampleInputEmail1";

        let EmailInput = new Element('input', ['form-control'], 'exampleInputEmail1', '').createElement()
        EmailInput.type = 'email';
        EmailInput.placeholder = 'Enter email'

        let EmailSmall = new Element('small', ["form-text", "text-muted"], 'emailHelp', "We'll never share your email with anyone else.").createElement()

        let EmailFragment = document.createDocumentFragment()
        EmailFragment.append(EmailLabel, EmailInput, EmailSmall)

        let EmailGroup = new Element('div', ['form-group', ''], '', EmailFragment).createElement()

        let PasswdLabel = new Element('label', ['1'], '', 'Password').createElement()
        PasswdLabel.for = "exampleInputPassword1";

        let PasswdInput = new Element('input', ['form-control', 'mb-2'], 'exampleInputPassword1', '').createElement()
        PasswdInput.type = 'password';
        PasswdInput.placeholder = 'Password'

        let PasswdFragment = document.createDocumentFragment()

        PasswdFragment.append(PasswdLabel, PasswdInput)

        let PasswdGroup = new Element('div', ['form-group', 'mb-3', 'mt-3'], '', PasswdFragment).createElement()

        let btn = new Element('button', ['btn', 'btn-primary'], 'login_submit', 'Submit').createElement()
        btn.type = 'submit'

        let LogInFormFragment=document.createDocumentFragment()
        LogInFormFragment.append(EmailGroup, PasswdGroup, btn)
        
        return LogInFormFragment

    }
    handleSumbit(event){
        event.preventDefault();
        let formInputs = [...event.target.elements];
        let dataAuthorization = {}
        dataAuthorization.email = formInputs[0].value
        dataAuthorization.password = formInputs[1].value
        
        let getToken = new Request(URL) 
        getToken.postLogin(dataAuthorization)
        .then(({data}) => {
           
            let modal = document.querySelector('#login_modal')
            let btnLogin = document.querySelector('#btn_login')
            let btnCreateCard = document.querySelector('#btn_createCard')

            btnLogin.style.display = 'none'
            
         
            btnCreateCard.classList.remove('noactive')
            
            modal.classList.remove('active')
            const pushCards = new Card().pushBordFromStorege()   
            

        })
        .catch(data => {
            alert('Invalid credentials')
            console.log('Enter valid data')})
    }
    


}