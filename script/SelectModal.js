import Modal from "./Modal.js";
import SelectForm from "./SelectForm.js";

export default class SelectModal extends Modal{
    constructor(id, classes, text) {
        super(id, classes, text);
    }

    createContent() {
        let selectform = new SelectForm(["select-form"], 'mainform').render();
        return selectform;
    }
}