export default {
    LOGIN_BTN: document.getElementById('btn_login'),
    ROOT: document.getElementById('root'),
    URL: "https://ajax.test-danit.com/api/v2/",
    BTN_CREATE_CARD: document.getElementById("btn_createCard"),
    NOITEMSTEXT : document.getElementById('no_cards'),
    BOARD:document.getElementById('board'),
    BTN_SETTING_CARD: document.getElementById('settingCard'),
    BTN_SEARCH: document.getElementById('button-search'), 
    BTN_APPLY: document.getElementById('button-apply'),

}
