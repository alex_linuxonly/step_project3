import Element from "./Elements.js";
import Request from "./Request.js";
import CONSTANTS from './constants.js';
import Modal from "./Modal.js";


const {
    URL
} = CONSTANTS

 export default class Form {
    constructor( classes, id){
        this.id = id;
        this.classes = classes;
        this.fields = this.createFields();
    }

    render(){
       
        let form = new Element('form', this.classes, this.id, this.fields).createElement()

        form.addEventListener("submit", this.handleSumbit.bind(this));

        return form;
    }


    handleSumbit(event){
        event.preventDefault();
        console.log('MO')

}
 


 }
