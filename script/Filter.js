import CONSTANTS from './constants.js'
const {BOARD} = CONSTANTS

function filterPriority (){
    const allCards = document.getElementsByClassName('priority');
    const selectPriority = document.getElementById('select-priority')
    const indexSelect = selectPriority.value
    for(let i=0; allCards.length > i;i++){
        allCards[i].parentElement.parentElement.parentElement.style.display = 'flex'
    }
    if(indexSelect !== 'all'){
        for(let i=0; allCards.length > i;i++){
            let valueSelect = selectPriority[indexSelect].textContent.trim()
            let valueDescription = allCards[i].textContent.split(':')[1].trim()
            // console.log(valueSelect===valueDescription);
            let card = allCards[i].parentElement.parentElement.parentElement
            if (valueSelect !==valueDescription ){
                card.style.display = 'none'
            }
        } 
    }
    
}
function filterDescription(){
    let allCards = document.getElementsByClassName('option_description')
    const searchDescription = document.getElementById('optionDescription')//// Добавить  HTML id = optionDescription
    const searchValue = searchDescription.value.replace(/\s/g, '').toLowerCase();
    console.log(searchValue);
    let allValueArr =[]
    for (let i=0; allCards.length > i;i++){
        let valueDescription = allCards[i].textContent.split(':')[1].replace(/\s/g, '').toLowerCase()
        allValueArr.push(valueDescription)
        allCards[i].parentElement.parentElement.parentElement.style.display = 'none'
    }
    console.log(allValueArr);
    function filterItems(searchValue) {
        let index = 0
         allValueArr.filter(function(el) {
            if( el.indexOf(searchValue) > -1){
                console.log(allCards[index].parentElement.parentElement.parentElement);
                console.log(el);
                allCards[index].parentElement.parentElement.parentElement.style.display='flex'
            }
            index ++
        })
      }
      filterItems(searchValue)
}
export function mainSerch(){
    const optionPriority = document.getElementsByClassName('priority');
    const selectPriority = document.getElementById('select-priority')
    const indexSelect = selectPriority.value
    ///////////////////////////////////
    const optionDescription = document.getElementsByClassName('option_description')
    const searchDescription = document.getElementById('optionDescription')
    const searchValue = searchDescription.value.replace(/\s/g, '').toLowerCase();
    /////////////////////////////////
    let allValueArr =[]
    for (let i=0; optionDescription.length > i;i++){
        let valueDescription = optionDescription[i].textContent.split(':')[1].replace(/\s/g, '').toLowerCase()
        allValueArr.push(valueDescription)
        optionDescription[i].parentElement.parentElement.parentElement.style.display = 'none'
    }
    function filterItems(searchValue) {
       let index = 0
         allValueArr.filter(function(el) {
            if( el.indexOf(searchValue) > -1){
                optionDescription[index].parentElement.parentElement.parentElement.style.display='flex'
            }
            index ++
        })
      }
      ////////////////////////////////
    for(let i=0; optionPriority.length > i;i++){
        optionPriority[i].parentElement.parentElement.parentElement.style.display = 'none'
    }
    if (indexSelect === 'all' && searchValue === ''){
        for(let i=0; optionPriority.length > i;i++){
            optionPriority[i].parentElement.parentElement.parentElement.style.display = 'flex'
        }
    } else if (searchValue.trim() !== ''){
        filterItems(searchValue)
        for(let i=0; optionPriority.length > i;i++){
              let  element = optionPriority[i].parentElement.parentElement.parentElement
              const valuePrioriti =optionPriority[i].textContent.split(':')[1].replace(/\s/g, '').toLowerCase()
              let valueSelect
              if (indexSelect !=='all') {
                valueSelect = selectPriority[indexSelect].textContent.trim()
              } 
              if (indexSelect ==='all') {
                  return
              }
               if (element.style.display === 'flex' && valuePrioriti !== valueSelect){
                   console.log(valuePrioriti,valueSelect);
                element.style.display = 'none'
              }
        }
    } else if (searchValue.trim() === ''){
        filterPriority()
    }
}

