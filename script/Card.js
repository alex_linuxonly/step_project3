import Element from "./Elements.js"
import LocalStorage from "./LocalStorage.js"
import Request from "./Request.js"
import CONSTANTS from './constants.js'
import SelectModal from './SelectModal.js'
import Modal from "./Modal.js"
import {VisitFormDantist, VisitFormTherapist, VisitFormCardiologist} from "./VisitForm.js";
 
const {
    URL,
    NOITEMSTEXT,
    BOARD,
    BTN_SETTING_CARD,
    ROOT,
    BTN_APPLY,
    BTN_SEARCH
} = CONSTANTS






export default class Card {
    constructor(objCard) {
        this.card = objCard
        
    } 
    pushBord() {
        if (this.card.length > 0) {
            NOITEMSTEXT.classList.remove('active')
            NOITEMSTEXT.classList.add('noactive')
        }
        this.card.forEach(elem => {
            let arrKeys = Object.keys(elem)
            let settingCard = new Element('button', ['btn', 'btn-secondary', 'btn-sm', 'settingCard'],`settingCard`,`<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-wrench" viewBox="0 0 16 16"><path d="M.102 2.223A3.004 3.004 0 0 0 3.78 5.897l6.341 6.252A3.003 3.003 0 0 0 13 16a3 3 0 1 0-.851-5.878L5.897 3.781A3.004 3.004 0 0 0 2.223.1l2.141 2.142L4 4l-1.757.364L.102 2.223zm13.37 9.019.528.026.287.445.445.287.026.529L15 13l-.242.471-.026.529-.445.287-.287.445-.529.026L13 15l-.471-.242-.529-.026-.287-.445-.445-.287-.026-.529L11 13l.242-.471.026-.529.445-.287.287-.445.529-.026L13 11l.471.242z"/>
          </svg>`).createElement()         
          settingCard.style.position = 'absolute'
          settingCard.style.top = '0'
          settingCard.style.right = '3%'
          ///////////////////////////////////////////////////////
          settingCard.addEventListener('click',()=>{
              const id  = settingCard.parentElement.getAttribute('data-atribute')
              
              const getObj = new LocalStorage('',[id]).getCartFromLocal()
              const doc = getObj[0].doctor
              const card = getObj[0]
            
            
              if(document.getElementById('editmodal')){
                  console.log('ok')
                document.getElementById('editmodal').remove()
              }
              ///////////////////////////////
            if (document.getElementById('modall') !== null){
                document.getElementById('modall').remove()  
            } 
            ///////////////////////////////////////    
            function fnNewCard (){
                let setBtn = new Element('span', ['btn', 'btn-primary', 'mt-2'], 'btn-set', 'Set').createElement()
                let newForm = document.getElementById('form_doctor')
                newForm.append(setBtn)
                document.getElementById("btn-submit").remove()
                let arrForm = newForm.childNodes[0].childNodes              
                /////////////////////////////
                arrForm.forEach((elem) =>{
                    let elemName = elem.name
                    Object.keys(card).forEach((key)=>{
                        if(elemName === key){
                            elem.value = card[key]
                        }
                    })
                }) 
                /////////////////////////////////////
                setBtn.addEventListener('click', ()=>{
                    let newCard = {}
                    arrForm.forEach((elem) =>{
                        let elemName = elem.name
                        Object.keys(card).forEach((key)=>{
                            if(elemName === key){
                                newCard[key] = elem.value
                            }
                        })
                    }) 
                    newCard.id = card.id
                    newCard.doctor = card.doctor
                   
                    const pushStorage = new LocalStorage(newCard).pushCardInLocal()
                    const deleteCard = document.getElementById(card.id)
                    deleteCard.id = 'deleteCard'
                    let pushuBoard = new Card([newCard]).pushBord()
                    document.getElementById('deleteCard').after(document.getElementById(card.id))
                    deleteCard.remove()
                    document.getElementById('editmodal').remove()
                    ///////////////////////////////////////////
                    let changeCard = new Request(URL)
                    changeCard.editCard(newCard.id, newCard)
                    .then((data)=>{
                        
                        
                    })
                    

                })
            }    
            if(doc === 'Dantist'){
                let form = new VisitFormDantist(['test1'], 'form_doctor').render()
                let window = new Modal('editmodal', ["modal", "modal1"], form)
                window.render()
                window.openModal()
                fnNewCard()
            }
          else if(doc === 'Therapist'){
                let form = new VisitFormTherapist(['test1'], 'form_doctor').render()
                let window = new Modal('editmodal', ["modal", "modal1"], form)
                window.render()
                window.openModal()
                fnNewCard()
            }
            else if(doc === 'Cardiologist'){
                let form = new VisitFormCardiologist(['test2'], 'form_doctor').render()
                let window = new Modal('editmodal', ["modal", "modal1"], form)
                window.render()
                window.openModal()
                fnNewCard()
            }


            
              let selectDoc = document.getElementById('selectDoc')
            //   selectDoc.selectedIndex = 1
            // const getObj = new Card('',[test]).getCartFromLocal()
            // console.log(getObj.doctor);   
            //   selectDoc.value ="Dantist"
            //   console.log(selectDoc.selectedIndex);



          })
         
            let local = new LocalStorage(elem).pushCardInLocal()
            let newCard = new Element('div', ['border', 'border-dark', 'card'],`${elem.id}`,``).createElement()
            newCard.style.width = 'auto'
            newCard.style.margin = '15px'
            newCard.style.position = 'relative'
            newCard.dataset.atribute = `${elem.id}`

            let cardName = new Element('p', ['m-3']).createElement()
            cardName.textContent = `Name: ${elem.fullname}`
            let cardDoctor = new Element('p', ['m-3']).createElement()
            cardDoctor.textContent = `Doctor: ${elem.doctor}`
            newCard.append(cardName, cardDoctor)

            let btn = new Element('button', ['btn', 'btn-success']).createElement()
            btn.dataset.toggle = "collapse"
            btn.ariaExpanded = "fals"
            btn.dataset.target = `#collapseCard${elem.id}`
            btn.textContent = 'see more...'

            newCard.append(btn)

            let collapse = new Element('div', ['collapse'], `collapseCard${elem.id}`, '').createElement()
            collapse.dataset.parent = '#accordion'
            collapse.style.width = '200px'
            let cardBody = new Element('div', ['card-body', 'm-2'], '', '').createElement()
            arrKeys.forEach(e => {
                if (e !== 'id' && e !== 'fullname' && e !== 'doctor') {
                    let cardOption = new Element('p', []).createElement()
                    cardOption.textContent = `${e}: ${elem[e]}`
                    cardBody.append(cardOption)
                    if(e === 'priority'){
                        cardOption.classList.add('priority')
                    }
                    if(e === 'description'){
                        cardOption.classList.add('option_description')
                    }


                }
            })

            let btnDelete = new Element('button', ['btn', 'btn-secondary', 'btn-sm', 'btn-delete-card'], ``, ``).createElement()
            btnDelete.textContent = 'Delete Card'
            btnDelete.textContent ='X'
            btnDelete.style.position = 'absolute'
            btnDelete.style.top = '0'
            btnDelete.style.right = '0'

            // cardBody.append(btnDelete,settingCard)
            collapse.append(cardBody)
            newCard.append(collapse,btnDelete,settingCard)
            BOARD.append(newCard)

            btnDelete.addEventListener('click', () => {
                let deleteCartInStorage = new LocalStorage('', [`${elem.id}`]).deleteCard()
                

                if ((BOARD.childNodes).length < 7) {
                    
                    NOITEMSTEXT.classList.remove('noactive')
                    NOITEMSTEXT.classList.add('active')
                }
                let deletes = new Request(URL).delete(elem.id)
                deletes.then((response) => {
                    
                })
                
                newCard.remove()
            })
        })
    }


    pushBordFromStorege() {
        let keys = Object.keys(localStorage)
       
        let arrCards = []
        keys.forEach(e => {
            if (JSON.parse(localStorage[e]).hasOwnProperty("fullname") && JSON.parse(localStorage[e]).hasOwnProperty("doctor")) {

                arrCards.push(JSON.parse(localStorage[e]))
            }
        })

        this.card = arrCards
        this.pushBord()
        if (keys.length > 0) {
            NOITEMSTEXT.classList.remove('active')
            NOITEMSTEXT.classList.add('noactive')
        }
    }

}



const allCardsId = {
    id : [29292]
}

