import Form from "./Form.js";
import Element from "./Elements.js";
import Request from "./Request.js";
import CONSTANTS from "./constants.js"
import Card from "./Card.js";

const { URL } = CONSTANTS

export {
    VisitForm,
    VisitFormDantist,
    VisitFormTherapist,
    VisitFormCardiologist
};

class VisitForm extends Form {
    constructor(classes, id ) {
        super(classes, id)
        
    }

    createFields() {
        
        let nameField = new Element('input', ['form-control', 'mt-2'], 'doc-name', '').createElement()
        nameField.placeholder = 'Name'
        nameField.name = 'fullname'

        let aimField = new Element('input', ['form-control', 'mt-2'], 'doc-aim', '').createElement()
        aimField.placeholder = 'Aim'
        aimField.name = 'aim'

        let priorityField = new Element('select', ['mt-2'], 'doc-prior', '').createElement()
        priorityField.name = 'priority'

        let defaultPriorityField = new Element('option', [''], 'doc-prior', 'Choose priority').createElement()
        defaultPriorityField.selected = true

        let optionSimple = new Element('option', [''], 'doc-prior', 'low').createElement()
        optionSimple.value = "low"
        let optionPriority = new Element('option', [''], 'doc-prior', 'normal').createElement()
        optionPriority.value = 'normal'
        let optionUrgent = new Element('option', [''], 'doc-prior', 'high').createElement()
        optionUrgent.value = 'high'
        priorityField.append(defaultPriorityField, optionSimple, optionPriority, optionUrgent)



        let shortDescriptionField = new Element('input', ['form-control', 'mt-2'], 'doc-field', '').createElement()
        shortDescriptionField.placeholder = 'Short description'
        shortDescriptionField.type = 'text'
        shortDescriptionField.name = 'description'





        let submitBtn = new Element('button', ['btn', 'btn-primary', 'mt-2'], 'btn-submit', 'Submit').createElement()

        
        let fragment = document.createDocumentFragment()
        
        

        fragment.append(nameField, aimField, priorityField, shortDescriptionField, this.createContent(), submitBtn)
       
        let newfragment = Array.prototype.slice.call(fragment.childNodes);

        submitBtn.addEventListener('click', (event)=>{
            event.preventDefault()
            
            this.handleSumbit(newfragment, this.getDoctor(), event.target)
        })

        
        let div = new Element('div', [''], 'forma', fragment).createElement()
        
        div.style.display = 'flex'
        div.style.flexDirection = 'column'
        return div
    }
    createContent() {
        return 'Hello'
    }
    handleSumbit(fragment, doc, target){        
        let window = target.parentElement.parentElement.parentElement.parentNode.parentNode
        document.getElementById('select_modal').remove()
        window.childNodes[0].childNodes[2][0].value = 'All'
        window.classList.remove('active')
        target.parentNode.remove()
        
        let card = {
            doctor:doc.doctor
        }
        
        fragment.forEach((element) => {
            if(element.tagName === 'INPUT' || element.tagName === 'SELECT'){
                // Object.defineProperty(card, element.name, {
                //     value: element.value,
                //   });
                card[element.name] = element.value
            }
        })
      
       
        let sendCard = new Request(URL)
        sendCard.post2(card)
        .then(response => response.json())
        .then(response => {
            let newArr = []
            
            newArr.push(response)
            let cards = new Card(newArr).pushBord()})
        

        
    }
}


class VisitFormDantist extends VisitForm {
    constructor(classes, id) {
        super(classes, id)
    }

    createContent() {
        let lastVisitText = new Element('p', ['mt-2', 'mb-0'], '', 'Last visit date').createElement()
        let lastVisitDate = new Element('input', ['form-control'], 'doc-date', '').createElement()
        lastVisitDate.type = 'date'
        lastVisitDate.name = 'date'
        let fragment = document.createDocumentFragment()
        fragment.append(lastVisitText, lastVisitDate)
        return fragment

    }
    getDoctor(){
        let user = {
            doctor:"Dantist"
        }
        return user
    }
}

class VisitFormTherapist extends VisitForm {
    constructor(classes, id) {
        super(classes, id)
        
        
    }

    createContent() {
        let age = new Element('input', ['mt-2', 'form-control'], '', '').createElement()
        age.placeholder = 'Age'
        age.name = 'age'
        return age

    }
    getDoctor(){
        let user = {
            doctor:"Therapist"
        }
        return user
    }
}

class VisitFormCardiologist extends VisitForm {
    constructor(classes, id) {
        super(classes, id)
    }

    createContent() {
        let simplePressure = new Element('input', ['mt-2', 'form-control'], '', '').createElement()
        simplePressure.placeholder = 'Pressure' 
        simplePressure.name = 'pressure'
        
        let indexBodyMass = new Element('input', ['mt-2', 'form-control'], '', '').createElement()
        indexBodyMass.placeholder = 'Index Mass of Body'
        indexBodyMass.name = 'indexmass'

        let illness = new Element('input', ['mt-2', 'form-control'], '', '').createElement()
        illness.type = "text"
        illness.placeholder = 'Illnesses'
        illness.name = 'illness'

        let age = new Element('input', ['mt-2', 'form-control'], '', '').createElement()
        age.placeholder = 'Age'
        age.name = 'age'

        const fragment = document.createDocumentFragment()
        fragment.append(simplePressure, indexBodyMass, illness, age)
        return fragment

    }
    getDoctor(){
        let user = {
            doctor:"Cardiologist"
        }
        return user
    }
}