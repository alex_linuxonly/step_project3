import Modal from "./Modal.js";
import LogInForm from "./LoginForm.js";

export default class LoginModal extends Modal{
    constructor(id, classes, text) {
        super(id, classes, text);
    }

    createContent() {
        let form = new LogInForm(["login-form"], '').render();
        return form;
    }

}