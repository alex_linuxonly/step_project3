class Visit{
    constructor(purpose, shortDescription, priority, patient){
        this.purpose = purpose,
        this.shortDescription = shortDescription,
        this.priority = priority,
        this.patient = patient
    }
}

class VisitDentist extends Visit{
    constructor(purpose, shortDescription, priority, patient, lastVisit){
        super(purpose, shortDescription, priority, patient)
        this.lastVisit = lastVisit;
    }
    
}

class VisitCardiologist extends Visit{
    constructor(purpose, shortDescription, priority, patient, pressure, bodyMassIndex, ilness, age){
        super(purpose, shortDescription, priority, patient)
        this.pressure = pressure,
        this.bodyMassIndex = bodyMassIndex,
        this.ilness = ilness,
        this.age = age
    }

}

class VisitTerapevt extends Visit{
    constructor(purpose, shortDescription, priority, patient, age){
        super(purpose, shortDescription, priority, patient)
        this.age = age
    }

}